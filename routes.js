/*
CREATE A ROUTE
"/greeting"

*/

const http=require('http')
let url=require("url")

// Creates a variable 'port' to store the port number
const port=4000

const server=http.createServer((request,response)=>{
	if(request.url=='/greeting'){
		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end('Hello Again')

		//Accesing the 'homepage' route returns a message of "This is homepage" 
	}else if(request.url=='/homepage'){
		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end('This is homepage')
	// All other routes will return a message of 'Page not available'

	}else{
		response.writeHead(404,{'Content-Type':'text/plain'})
		response.end('Page not available')
	}
})

// uses the "Server" and "port"  variables created above
server.listen(port)
console.log(`Server now accesible at localhost:${port}`)